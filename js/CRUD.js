'use strict';


//sorte de constructeur
function CrudJson(url, resourceName) {
    //création d'une variable privée
    var serveurName = url;
    var ressource = resourceName;

    //this sert juste à dire que c'est public
    this.lastResult = null;

    //ajoute une note dans le JSON via REST
    //au début on passait par la console et on passait getFormValues en paramètre de CreateNote()
    this.create = function(params) {
        var xhr = new XMLHttpRequest();
        //3eme param = asynchrone ou non
        xhr.open('POST', serveurName + ressource, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        //sans attendre la réponse, on délègue l'action à faire à la fonction anonyme
        //plusieurs états d'exécution
        //la portée de la variable xhr est dans toute la fonction parent (ici, c'est loadNote)
        xhr.onreadystatechange = function(response) {
            console.log(xhr.status, xhr.readyState);
            //on n'écrit dans la console qu'à la fin, quand tout s'est bien passé
            if (xhr.readyState == XMLHttpRequest.DONE && (xhr.status >= 200 && xhr.status < 300)) {
                //attention xhr.response est une string
                /*var mesNotes = JSON.parse(xhr.response);
                var retransJSON = JSON.stringify(mesNotes);
                console.log(xhr.response, mesNotes, retransJSON);*/

                var obj = JSON.parse(xhr.response);
                if (undefined != params.clbk) params.clbk(obj);
                //clbk(document.querySelector('#list-note'), maNote);
            }
        };
        xhr.send(JSON.stringify(params.postElement));
    }

    //on passe un objet params générique, qui contient des propriétés
    //comme ca on n'a qu'un objet en paramètre
    this.read = function loadNote(params) {
        var xhr = new XMLHttpRequest();
        //si l'id n'est pas null, je le concatène, sinon, je n'ajoute rien
        var uri = serveurName + ressource + '/' + ((undefined !== params.id) ? params.id : '')
            //3eme param = asynchrone ou non
        xhr.open('GET', uri, true);
        //sans attendre la réponse, on délègue l'action à faire à la fonction anonyme
        //plusieurs états d'exécution
        //la portée de la variable xhr est dans toute la fonction parent (ici, c'est loadNote)
        xhr.onreadystatechange = function(response) {
            console.log(xhr.status, xhr.readyState);
            //on n'écrit dans la console qu'à la fin, quand tout s'est bien passé
            if (xhr.readyState == XMLHttpRequest.DONE && (xhr.status >= 200 && xhr.status < 300)) {
                console.log(xhr.status, xhr.readyState);
                //attention xhr.response est une string
                var mesNotes = JSON.parse(xhr.response);
                /* var retransJSON = JSON.stringify(mesNotes);
                console.log(xhr.response, mesNotes, retransJSON);*/

                /*avec innerHTML, on ajoute à la volée des objets ==> pas top car on bousille le DOM jusqu'au noeud #list-note
                ==> tous les événements associés à chaque noeud du DOM dans le conteneur #liste-note  sont niqués (et du coup, il faut tout réindexer)
                var listNote = document.querySelector('#list-note');
                mesNotes.forEach(function(element, ind) {
                     listNote.innerHTML += '<div><div style="text-align:center">' + ind + ':' + element.nom +
                         '</div></div>';
                });*/

                //meilleur pratique, faire des append dans le DOM ==> DOM pas bousillé


                //mesNotes.forEach(function(element) {
                //addNote(listNote, element);
                if (undefined !== params.clbk) params.clbk(mesNotes);
                //});
            }
        };
        xhr.send();
    }

    //détruit une note dans le JSON via REST
    //clbk est une fonction de callback, qu'on appelle à la fin
    this.delete = function deleteNote(params) {
        var uri = serveurName + ressource + '/' + params.id;

        var xhr = new XMLHttpRequest();
        //3eme param = asynchrone ou non
        xhr.open('DELETE', uri, true);
        //sans attendre la réponse, on délègue l'action à faire à la fonction anonyme
        //plusieurs états d'exécution
        //la portée de la variable xhr est dans toute la fonction parent (ici, c'est loadNote)
        xhr.onreadystatechange = function(response) {
            console.log(xhr.status, xhr.readyState);
            //on n'écrit dans la console qu'à la fin, quand tout s'est bien passé
            if (xhr.readyState == XMLHttpRequest.DONE && (xhr.status >= 200 && xhr.status < 300)) {
                //attention xhr.response est une string
                //ancienne facon de faire, avant la mise en place de la fonction de callback
                // var mesNotes = JSON.parse(xhr.response);
                // var retransJSON = JSON.stringify(mesNotes);
                // console.log(xhr.response, mesNotes, retransJSON);

                if (undefined !== params.clbk) params.clbk();
            }
        };
        xhr.send();
    }
}