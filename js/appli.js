//on s'interdit la permissivité ==> empêche de trop pourrir le code
'use strict';
const ADR_REST_SERVEUR = 'http://localhost:5000/';

var crudJson = new CrudJson(ADR_REST_SERVEUR, 'notes')

function isLoaded() {
    console.log('Code JS chargé');
    var maBalise = document.getElementById('status');
    maBalise.innerText = 'JS chargé';
    maBalise.style.backgroundColor = 'GREEN';

    addEventsForNoteForm();

    //on charge les notes du JSON dans le conteneur list-notes
    //on crée un objet qu'on passera en paramètre unique aux méthodes de l'objet crudJson
    var parametresReadAll = {
        clbk: function(notes) {
            var list = document.querySelector("#list-note")
                //uneNoteDeLarray est l'élément renvoyé par forEach à chaque tour de boucle
            notes.forEach(function(uneNoteDeLarray) {
                addNote(list, uneNoteDeLarray)
            });
        }
    };
    crudJson.read(parametresReadAll);
    //loadNote();
}
isLoaded();

function addEventsForNoteForm() {
    console.log('add events for note forms');

    //button#saveNote  on s'assure d'avoir le node html d'un bouton qui porte la classe saveNote
    var button = document.querySelector('button#saveNote');
    button.addEventListener('click', saveNote);

    //autre moyen de sélectionner un node du html
    button = document.getElementById('resetNote');
    button.addEventListener('click', resetNote);

    //    console.log(button);
}

/*-----------Gestion du formulaire de notes-----------------*/

function saveNote() {
    var noteDatas = getFormValues();
    if (undefined === noteDatas.id)

    //createNote(noteDatas);
        crudJson.create({
        postElement: noteDatas,
        clbk: function(note) {
            var list = document.getElementById('list-note');
            addNote(list, note);
        }
    });
    //console.log('save note by event');
    else
    //createNote(noteDatas);
        crudJson.update({
        postElement: noteDatas,
        clbk: function(note) {
            var list = document.getElementById('list-note');
            var newNote = createNodeNote(note);
            var oldNote = list.querySelector('#note-' + note.id);
            list.replaceChild(newNote, oldNote);
        }
    });

}

//vide le fomulaire
function resetNote() {
    console.log('vidange note by event');
    document.forms.newNote.reset();
}

//envoie la note dans le serveur
function createNodeNote(note) {
    var noteDiv = document.createElement('div');
    noteDiv.setAttribute('class', 'vue-note');
    noteDiv.setAttribute('id', 'note-' + note.id);
    //on ajoute la croix dans chaque note
    noteDiv.innerHTML = '<div class="close-icon-block"><img src="img/close.svg" /></div>' + note.id + ':' + note.nom;

    //on cherche les élements close-icon-block dans ma noteDiv
    noteDiv.querySelector('.close-icon-block>img').addEventListener('click', removeNote);

    //evt => équivalent de function(evt) mais gère en plus le deférencement du this
    noteDiv.addEventListener('click', evt => {
        console.log('je viens de cliquer sur le log');
        crudJson.read({ id: note.id, clbk: putNoteInForm })

    });
    return noteDiv;
}

//envoie la note au serveur
//ajoute la note au DOM
function addNote(listNote, note) {
    var noteDiv = createNodeNote(note);
    listNote.appendChild(noteDiv);
}

function putNoteInForm(note) {
    console.log(note);
    var form = document.forms.newNote;
    form.querySelector('input#nom-note').value = note.nom;

    form.querySelector('input#date-note').value = note.date;

    form.querySelector('input#heure-note').value = note.heure;

    form.querySelector('#description-note').value = note.description;

    //met le numéro de la note dans l'élément caché du html
    form.querySelector('#idnote').value = note.id;

}

//récupère le contenu entré par l'utilisateur
function getFormValues() {
    var monForm = document.forms.newNote;
    //on crée une instance d'un objet JavaScript (sans qu'il y ait de classe spécifique qui soit créée)
    var returnObject = {};

    returnObject.date = monForm.querySelector('#date-note').value;
    returnObject.heure = monForm.querySelector('#heure-note').value;
    returnObject.description = monForm.querySelector('#description-note').value;
    returnObject.nom = monForm.querySelector('#nom-note').value;

    //si la valeur est vide, on est sur une création de note, sinon, on est sur une modification
    if (monForm.querySelector('#idnote').value != '')
        returnObject.id = monForm.querySelector('#idnote').value;

    console.log(returnObject);
    return returnObject;
}


function removeNote(evt) {
    console.log('je suis dans le remove note');
    //on remonte de 2 niveaux par rapport à l'image Close (conteneur 1 = une div, conteneur 2 = sa note)
    var idNote = evt.target.parentElement.parentElement.id;
    var array = idNote.split('-');
    console.log(array);
    idNote = array[1];
    console.log(idNote);
    //on a mis en place une fonction de callback, 
    //comme ca, si la destruction de l'idNote ne se passe pas bien, on ne détruit pas la note
    //deleteNote(idNote, function() { evt.target.parentElement.parentElement.remove() });

    //demande la confirmation avant la suppression
    if (confirm('Etes-vous sur id: ' + idNote))
        crudJson.delete({
            id: idNote,
            clbk: function() {
                evt.target.parentElement.parentElement.remove();
            }
        });
}
//tous les enfants de la classe close-icon-block dans l'objet note-2
//document.querySelector('#note-2 .close-icon-block>*').addEventListener('click', removeNote);