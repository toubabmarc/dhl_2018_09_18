// 'use strict';


function CrudJson(url, ressourceName) {
    var _vm = this;
    var serveur = url;
    var ressource = ressourceName;

    this.lastResult = null;

    //read a besoin ou non de l'id (et jamais de l'objet complet)  ==> on teste sur params.id
    this.read = function(params) {
        var xhr = new XMLHttpRequest();
        var uri = url + ressource + '/' + ((undefined !== params.id) ? params.id : '')

        xhr.open('GET', uri, true);

        xhr.onreadystatechange = (function(response) {
                console.log(xhr.status, xhr.readyState);

                if (xhr.readyState == XMLHttpRequest.DONE && (xhr.status >= 200 && xhr.status < 300)) {
                    //attention le this n'est plus le this du CrudJson, mais celui du xhr(on lui a passé la main)
                    console.log(this);
                    var mesNotes = JSON.parse(xhr.response);
                    // mesNotes.forEach(function(element) {
                    if (undefined !== params.clbk) params.clbk(mesNotes);
                    //});
                }
            }).bind(this) //le bind permet de lier le xhr (et donc les infos sur le retour de la fonction) au this du CrudJson
            //attention du coup à bien mettre des parenthèses autour de la fonction xhr
        xhr.onreadystatechange = response => {
            console.log(xhr.status, xhr.readyState);

            if (xhr.readyState == XMLHttpRequest.DONE && (xhr.status >= 200 && xhr.status < 300)) {
                console.log(this);
                var mesNotes = JSON.parse(xhr.response);
                // mesNotes.forEach(function(element) {
                if (undefined !== params.clbk) params.clbk(mesNotes);
                //});
            }
        }

        xhr.send();
    }

    //create a besoin de l'objet complet pour l'envoyer au serveur ==> on utilise params.postElement (en fin de fonction)
    //par contre, il n'a pas besoi de l'id, vu qu'il n'existe pas encore
    this.create = function(params) {
            var xhr = new XMLHttpRequest();
            xhr.open('POST', serveur + ressource, true);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.onreadystatechange = (function(response) {
                console.log(xhr.status, xhr.readyState);
                if (xhr.readyState == XMLHttpRequest.DONE && (xhr.status >= 200 && xhr.status < 300)) {
                    var obj = JSON.parse(xhr.response);
                    // addNote(document.querySelector('#list-note'), maNote);
                    console.log('no link', this);
                    console.log('vm link', _vm);
                    this.lastResult = obj;
                    if (undefined !== params.clbk) params.clbk(obj);
                    // clbk(document.querySelector('#list-note'), maNote);
                }
            }).bind(this);
            xhr.send(JSON.stringify(params.postElement));
        }
        //delete a besoin de l'id uniquement, pas de l'objet complet ==> on utilise params.id
    this.delete = function(params) {
        var xhr = new XMLHttpRequest();
        var uri = url + ressource + '/' + params.id;

        xhr.open('DELETE', uri, true);
        xhr.onreadystatechange = function(response) {
            console.log(xhr.status, xhr.readyState);

            if (xhr.readyState == XMLHttpRequest.DONE && (xhr.status >= 200 && xhr.status < 300)) {
                console.log(this);
                var mesNotes = JSON.parse(xhr.response);
                // mesNotes.forEach(function(element) {
                if (undefined !== params.clbk) params.clbk(mesNotes);
                //});
            }
        };
        xhr.send();
    }

    //update a besoin de l'objet complet ==> on utilie params.postElement et là-dedans, on va chercher l'id
    this.update = function(params) {
        var xhr = new XMLHttpRequest();
        var uri = url + ressource + '/' + params.postElement.id;

        xhr.open('PUT', uri, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function(response) {
            console.log(xhr.status, xhr.readyState);

            if (xhr.readyState == XMLHttpRequest.DONE && (xhr.status >= 200 && xhr.status < 300)) {
                //    console.log(this);
                var mesNotes = JSON.parse(xhr.response);
                _vm.lastResult = mesNotes;
                // mesNotes.forEach(function(element) {
                if (undefined !== params.clbk) params.clbk(mesNotes);
                //});
            }
        };
        xhr.send(JSON.stringify(params.postElement));
    }

}